from django.urls import path
from .views import DehqonAdd, Home

urlpatterns = [
    path('', Home, name = 'home'),
    path('dehqon', DehqonAdd, name = 'DehqonAdd')
]