from django.db import models

# Create your models here.
class Viloyat(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name

class Tuman(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name
class Malumotingiz(models.Model):
	name = models.CharField(max_length=255)

	def __str__(self):
		return self.name

class MaxsulotIshlabChiqarishTuri(models.Model):
	name = models.CharField(max_length=255)

	def __str__(self):
		return self.name

class XujalikQushimchaFaoliyati(models.Model):
	name = models.CharField(max_length=255)

	def __str__(self):
		return self.name

class SugorishTarmoq(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name

class SugorishUchunSuvManbasi(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name
class MaxsulotYetishtirishUchunModdiyTexnikBaza(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class ModdiyTexnikBazaXaridQilishMuommosi(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name

class XizmatKursatishKorxonalarTexnikalari(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name


class EkinTuri(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class MaxsulotTuri(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class HosilSaqlashJoyi(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class SotganJoy(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class SaqlashUsuli(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class QuritganYokiQaytaIshlaganUsul(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class KreditMaqsadi(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class QuyliganGarov(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class KreditOlmayXarajatlarniTulash(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class MoliyalashtirishniBaholash(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name

class MoliyaviyIshlarniKimYuritadi(models.Model):
	name = models.CharField(max_length=100)

	def __str__(self):
		return self.name


class IshYuritishBuyichaYangiMalumotlar(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class MashgulotTuri(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class MashgulotTuriQanaqaBulishiIstaysiz(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class SurovnomaPasport(models.Model):
	viloyat = models.ForeignKey(Viloyat, on_delete=models.CASCADE, null=True)
	tuman = models.ForeignKey(Tuman, on_delete=models.CASCADE)
	hudud = models.CharField(max_length=100)
	mahalla = models.CharField(max_length=100)
	raxbar_ism_sharf = models.CharField(max_length=100)
	xujalig_nomi = models.CharField(max_length=255)
	hududda_joylashgan_manzil = models.CharField(max_length=255)
	raxbar_yoshi = models.IntegerField()
	malumotingiz = models.ForeignKey(Malumotingiz, on_delete=models.CASCADE)
	yuridek_maqomga_egami = models.BooleanField()
	xujalik_tashkil_topgan_yil = models.DateField()
	xujalik_oila_azolar_soni = models.IntegerField()
	xujalik_turi = models.ForeignKey(MaxsulotIshlabChiqarishTuri, on_delete=models.CASCADE)
	qushimcha_faoliyati_turi = models.ForeignKey(XujalikQushimchaFaoliyati, on_delete=models.CASCADE)
	umumiy_maydon = models.IntegerField()
	ekin_maydon = models.IntegerField()
	sugoriladigan_maydon = models.IntegerField()
	lalmi_maydon = models.IntegerField()
	boshqa_maydon = models.IntegerField()
	yer_maydon_uzgarganmi = models.BooleanField()
	uzgargan_yil = models.DateField()
	qanchaga_uzgargan = models.IntegerField()
	sugorish_tarmoq = models.ForeignKey(SugorishTarmoq, on_delete=models.CASCADE)
	sugorish_manbasi = models.ForeignKey(SugorishUchunSuvManbasi, on_delete=models.CASCADE)
	sugorish_bilan_taminlangan_qismi_foizda = models.IntegerField()
	yer_maydon_tuman_markazidan_qancha_uzoq = models.IntegerField()
	urug_sotib_olish_asosiy_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='urug_sotib_olish_asosiy_manba')
	urug_sotib_olish_ikkilamchi_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='urug_sotib_olish_ikkilamchi_manba')
	ugit_sotib_olish_asosiy_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='ugit_sotib_olish_asosiy_manba')
	ugit_sotib_olish_ikkilamchi_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='ugit_sotib_olish_ikkilamchi_manba')
	pestisidvagerbisid_sotib_olish_asosiy_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='pestisidvagerbisid_sotib_olish_asosiy_manba')
	pestisidvagerbisid_sotib_olish_ikkilamchi_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='pestisidvagerbisid_sotib_olish_ikkilamchi_manba')
	yoqilgi_sotib_olish_asosiy_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='yoqilgi_sotib_olish_asosiy_manba')
	yoqilgi_sotib_olish_ikkilamchi_manba = models.ForeignKey(MaxsulotYetishtirishUchunModdiyTexnikBaza, on_delete=models.CASCADE, related_name='yoqilgi_sotib_olish_ikkilamchi_manba')
	ur_naqd_pul_yetishmasligi = models.BooleanField()
	ur_qiymat_balandligi = models.BooleanField()
	ur_bozorda_tanqis = models.BooleanField()
	ur_sifatsiz = models.BooleanField()
	ur_transport_muammosi = models.BooleanField()
	ur_muammo_yuq = models.BooleanField()
	ur_boshqa = models.BooleanField()
	b_naqd_pul_yetishmasligi = models.BooleanField()
	b_qiymat_balandligi = models.BooleanField()
	b_bozorda_tanqis = models.BooleanField()
	b_sifatsiz = models.BooleanField()
	b_transport_muammosi = models.BooleanField()
	b_muammo_yuq = models.BooleanField()
	b_boshqa = models.BooleanField()
	ug_naqd_pul_yetishmasligi = models.BooleanField()
	ug_qiymat_balandligi = models.BooleanField()
	ug_bozorda_tanqis = models.BooleanField()
	ug_sifatsiz = models.BooleanField()
	ug_transport_muammosi = models.BooleanField()
	ug_muammo_yuq = models.BooleanField()
	ug_boshqa = models.BooleanField()
	pg_naqd_pul_yetishmasligi = models.BooleanField()
	pg_qiymat_balandligi = models.BooleanField()
	pg_bozorda_tanqis = models.BooleanField()
	pg_sifatsiz = models.BooleanField()
	pg_transport_muammosi = models.BooleanField()
	pg_muammo_yuq = models.BooleanField()
	pg_boshqa = models.BooleanField()
	y_naqd_pul_yetishmasligi = models.BooleanField()
	y_qiymat_balandligi = models.BooleanField()
	y_bozorda_tanqis = models.BooleanField()
	y_sifatsiz = models.BooleanField()
	y_transport_muammosi = models.BooleanField()
	y_muammo_yuq = models.BooleanField()
	y_boshqa = models.BooleanField()
	texnika_turi = models.ForeignKey(XizmatKursatishKorxonalarTexnikalari, on_delete = models.CASCADE)
	son1 = models.IntegerField()
	bahosi1 = models.FloatField()
	son2 = models.IntegerField()
	bahosi2 = models.FloatField()
	xaydov_traktor_1 = models.BooleanField()
	xaydov_traktor_2 = models.BooleanField()
	xaydov_traktor_3 = models.BooleanField()
	xaydov_traktor_4 = models.BooleanField()
	xaydov_traktor_5 = models.BooleanField()
	xaydov_traktor_6 = models.BooleanField()
	pluglar_1 = models.BooleanField()
	pluglar_2 = models.BooleanField()
	pluglar_3 = models.BooleanField()
	pluglar_4 = models.BooleanField()
	pluglar_5 = models.BooleanField()
	pluglar_6 = models.BooleanField()
	seyalka_1 = models.BooleanField()
	seyalka_2 = models.BooleanField()
	seyalka_3 = models.BooleanField()
	seyalka_4 = models.BooleanField()
	seyalka_5 = models.BooleanField()
	seyalka_6 = models.BooleanField()
	kultivator_1 = models.BooleanField()
	kultivator_2 = models.BooleanField()
	kultivator_3 = models.BooleanField()
	kultivator_4 = models.BooleanField()
	kultivator_5 = models.BooleanField()
	kultivator_6 = models.BooleanField()
	galla_kanbayn_1 = models.BooleanField()
	galla_kanbayn_2 = models.BooleanField()
	galla_kanbayn_3 = models.BooleanField()
	galla_kanbayn_4 = models.BooleanField()
	galla_kanbayn_5 = models.BooleanField()
	galla_kanbayn_6 = models.BooleanField()
	yuk_mashina_1 = models.BooleanField()
	yuk_mashina_2 = models.BooleanField()
	yuk_mashina_3 = models.BooleanField()
	yuk_mashina_4 = models.BooleanField()
	yuk_mashina_5 = models.BooleanField()
	yuk_mashina_6 = models.BooleanField()
	boshqa_1 = models.BooleanField()
	boshqa_2 = models.BooleanField()
	boshqa_3 = models.BooleanField()
	boshqa_4 = models.BooleanField()
	boshqa_5 = models.BooleanField()
	boshqa_6 = models.BooleanField()
	qoramollar_soni = models.IntegerField()
	qoramollar_urtacha_qiymati = models.FloatField()
	undan_sigirlar_soni = models.IntegerField()
	undan_sigirlar_urtacha_qiymati = models.FloatField()
	quylar_soni = models.IntegerField()
	quylar_urtacha_qiymati = models.FloatField()
	echkilar_soni = models.IntegerField()
	echkilar_urtacha_qiymati = models.FloatField()
	otlar_soni = models.IntegerField()
	otlar_urtacha_qiymati = models.FloatField()
	tovuqlar_soni = models.IntegerField()
	tovuqlar_urtacha_qiymati = models.FloatField()
	turi = models.ForeignKey(EkinTuri, on_delete=models.CASCADE)
	ekin_maydon_ga = models.FloatField()
	yalpi_hosil = models.FloatField()
	hosildorlik = models.FloatField()
	asosiy_takroriy = models.BooleanField()
	maxsulot_turi = models.ForeignKey(MaxsulotTuri, on_delete=models.CASCADE, related_name='maxsulot_turi')
	sotishda_asosiy_muammo_1 = models.BooleanField()
	sotishda_asosiy_muammo_2 = models.BooleanField()
	sotishda_asosiy_muammo_3 = models.BooleanField()
	sotishda_asosiy_muammo_4 = models.BooleanField()
	sotishda_asosiy_muammo_5 = models.BooleanField()
	sotishda_asosiy_muammo_6 = models.BooleanField()
	sotishda_asosiy_muammo_7 = models.BooleanField()
	sotishda_asosiy_muammo_8 = models.BooleanField()
	maxsulot_turi_2 = models.ForeignKey(MaxsulotTuri, on_delete=models.CASCADE, related_name='maxsulot_turi_2')
	hosil_saqlash_joyi = models.ForeignKey(HosilSaqlashJoyi, on_delete=models.CASCADE)
	uzingiz_eksport_qilish_xoxlaysizmi = models.BooleanField()
	eksport_qilishni_bilasizmi = models.BooleanField()
	eksport_tizim_haqida_malumot_bormi = models.BooleanField()
	maxsulot_turi_3 = models.ForeignKey(MaxsulotTuri, on_delete=models.CASCADE, related_name='maxsulot_turi_3')
	maxsulot_miqdori = models.FloatField()
	maxsulot_saqlab_keyin_sotdingizmi = models.BooleanField()
	sotgan_joy_manzili = models.ForeignKey(SotganJoy, on_delete=models.CASCADE)
	saqlash_usuli = models.ForeignKey(SaqlashUsuli, on_delete=models.CASCADE)
	quritish_yoki_qayta_ishladingizmi = models.BooleanField()
	quritish_yoki_qayta_ishlash_usuli = models.ForeignKey(QuritganYokiQaytaIshlaganUsul, on_delete=models.CASCADE)
	maxsulot_turi_4 = models.ForeignKey(MaxsulotTuri, on_delete=models.CASCADE, related_name='maxsulot_turi_4')
	pishib_yetiganda_sotilgan_miqdori = models.FloatField()
	urtacha_bahosi_1 = models.FloatField()
	saqlab_turgan_holda_sotilgan_miqdori = models.FloatField()
	urtacha_bahosi_2 = models.FloatField()
	quritib_yoki_qayta_ishlagan_sotilgan_miqdori = models.FloatField()
	urtacha_bahosi_3 = models.FloatField()
	hisob_raqam_bormi = models.BooleanField()
	qaysi_bankda = models.CharField(max_length=255)
	necha_yildan_buyon = models.FloatField()
	sungi_3_yil_ichida_qisqa_muddat_kredit = models.BooleanField()
	necha_marta_1 = models.IntegerField()
	maqsad_1 = models.ForeignKey(KreditMaqsadi, on_delete=models.CASCADE, related_name = 'maqsad_1')
	miqdor_1 = models.FloatField()
	muddat_1 = models.FloatField()
	foiz_1 = models.FloatField()
	oylik_tulov_1 = models.FloatField()
	quyilgan_gorov_1 = models.ForeignKey(QuyliganGarov, on_delete = models.CASCADE, related_name='quyilgan_gorov_1')
	quyilgan_garov_kredit_miqdori_foiz_nisbatida_1 = models.FloatField()
	olingan_vaqt_1 = models.DateField()
	tulash_uchun_qolgan_summa_1 = models.FloatField()
	kredit_olmay_xarajatlarni_tulash = models.ForeignKey(KreditOlmayXarajatlarniTulash, on_delete=models.CASCADE)
	sungi_3_yil_ichida_uzoq_muddat_kredit = models.BooleanField()
	necha_marta_2 = models.IntegerField()
	maqsad_2 = models.ForeignKey(KreditMaqsadi, on_delete=models.CASCADE, related_name = 'maqsad_2')
	miqdor_2 = models.FloatField()
	muddat_2 = models.FloatField()
	foiz_2 = models.FloatField()
	oylik_tulov_2 = models.FloatField()
	quyilgan_gorov_2 = models.ForeignKey(QuyliganGarov, on_delete = models.CASCADE, related_name='quyilgan_gorov_2')
	quyilgan_garov_kredit_miqdori_foiz_nisbatida_2 = models.FloatField()
	olingan_vaqt_2 = models.DateField()
	tulash_uchun_qolgan_summa_2 = models.FloatField()
	uskina_sotib_olish_uchun_uz_mablaglarim_yetarli_emas = models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='uskina_sotib_olish_uchun_uz_mablaglarim_yetarli_emas')
	mahaliy_bankda_hisobim_yuq = models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='mahaliy_bankda_hisobim_yuq')
	sotib_olish_uchun_kredit_olish_qiyin = models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='sotib_olish_uchun_kredit_olish_qiyin')
	beriladigan_muddat_qisqa= models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='beriladigan_muddat_qisqa')
	foizi_juda_baland = models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='foizi_juda_baland')
	gorov_talab_qilmoqda = models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='gorov_talab_qilmoqda')
	dehqon_xujaligi_daromadi_kredit_tulashga_yetmaydi = models.ForeignKey(MoliyalashtirishniBaholash, on_delete=models.CASCADE, related_name='dehqon_xujaligi_daromadi_kredit_tulashga_yetmaydi')
	moliyavir_ishlarni_kim_yuritadi = models.ForeignKey(MoliyaviyIshlarniKimYuritadi, on_delete=models.CASCADE)
	siz_moliya_sohasida_uquv_mashgulotiga_qatnashgansizmi = models.BooleanField()
	yangi_malumot_olish_joyi = models.ForeignKey(IshYuritishBuyichaYangiMalumotlar, on_delete=models.CASCADE)
	uquv_treninglarda_qatnashganmisiz = models.BooleanField()
	treningda_olib_borilgan_mashgulot_turi = models.ForeignKey(MashgulotTuri, on_delete=models.CASCADE)
	uqiganlarni_amalda_qulladizmi = models.BooleanField()
	mashgulot_turi_qanaqa_bulishini_istaysiz = models.ForeignKey(MashgulotTuriQanaqaBulishiIstaysiz, on_delete=models.CASCADE)




	