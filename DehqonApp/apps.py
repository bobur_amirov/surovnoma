from django.apps import AppConfig


class DehqonappConfig(AppConfig):
    name = 'DehqonApp'
