from django.shortcuts import render, redirect, HttpResponseRedirect
from .forms import DehqonForm
from django.urls import reverse


def Home(request):
    return render(request, 'home.html')


def DehqonAdd(request):
    if request.method == 'POST':
        form = DehqonForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')

    else:

        form = DehqonForm()


    return render(request, 'dehqonadd.html', {'form': form})