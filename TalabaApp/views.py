from django.shortcuts import render, redirect
from .forms import TalabaForm

# Create your views here.

# def Home(request):
#     return render(request, 'home.html')

def TalabaAdd(request):
    if request.method == 'POST':
        form = TalabaForm(request.POST)
        if form.is_valid():
            form.save()
            # my_talaba = form.save(commit=False)
            # my_talaba.save()
            return redirect('home')

    else:

        form = TalabaForm()


    return render(request, 'talabaadd.html', {'form': form})