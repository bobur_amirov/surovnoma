from django.db import models

# Create your models here.
class Fakultet(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class DehqonTalaba(models.Model):
    i_f_sh = models.CharField(max_length=100)
    fakultet = models.ForeignKey(Fakultet, on_delete=models.CASCADE)
    manzil = models.CharField(max_length=255)
    ix_dehqonchilik = models.BooleanField()
    ix_chorvachilik = models.BooleanField()
    ix_qayta_islash = models.BooleanField()
    ix_saqlash = models.BooleanField()
    ix_shaxar_atrofi = models.BooleanField()
    ix_boshqalar = models.BooleanField()
    sh_shaxsiy_tomarqa = models.BooleanField()
    sh_shirkat = models.BooleanField()
    sh_dehqon_xujaligi = models.BooleanField()
    sh_fermer_xujaligi = models.BooleanField()
    sh_boshqalar = models.BooleanField()
    r_yer_maydoni = models.BooleanField()
    r_suv = models.BooleanField()
    r_sugorish_tizimi = models.BooleanField()
    r_ishlov_mexanizimlari = models.BooleanField()
    r_shaxsiy_texnika = models.BooleanField()
    r_arenda = models.BooleanField()
    r_moliya = models.BooleanField()
    r_bank_krediti = models.BooleanField()
    r_materal_va_ugit = models.BooleanField()
    r_mutaxassis_maslahati = models.BooleanField()
    r_boshqalar = models.BooleanField()
    s_texnik_ekinlar = models.BooleanField()
    s_moyliy_ekinlar = models.BooleanField()
    s_rang_beruvchi = models.BooleanField()
    s_efir = models.BooleanField()
    s_poliz = models.BooleanField()
    s_galla = models.BooleanField()
    s_yem_hashak = models.BooleanField()
    s_kuchat_tizimi = models.BooleanField()
    s_uzumchilik = models.BooleanField()
    s_meva_va_sabzavot = models.BooleanField()
    s_kup_va_qisqa = models.BooleanField()
    s_noananaviy = models.BooleanField()
    s_gulchilik = models.BooleanField()
    s_issiqxona = models.BooleanField()
    s_max_qayta_ishlash = models.BooleanField()
    s_boshqalar = models.BooleanField()
    l_yem_hashak = models.BooleanField()
    l_galla = models.BooleanField()
    l_arpa = models.BooleanField()
    l_dorivor = models.BooleanField()
    l_max_qayta_ishlash = models.BooleanField()
    l_boshqalar = models.BooleanField()
    ch_yirik_shoxli = models.BooleanField()
    ch_kichik_shoxli = models.BooleanField()
    ch_parandachilik = models.BooleanField()
    ch_asal_arichilik = models.BooleanField()
    ch_baliqchilik = models.BooleanField()
    ch_pillachilik = models.BooleanField()
    ch_qayta_ishlash = models.BooleanField()
    ch_boshqalar = models.BooleanField()
    m_bank_kredit = models.BooleanField()
    m_sugurta = models.BooleanField()
    m_sherikchilik = models.BooleanField()
    m_agrosanoat_klasteri = models.BooleanField()


    def __str__(self):
        return self.i_f_sh