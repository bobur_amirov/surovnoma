from django.forms import ModelForm
from .models import DehqonTalaba


class TalabaForm(ModelForm):
    class Meta:
        model = DehqonTalaba
        fields = '__all__'


    def __init__(self, *args, **kwargs):
        super(TalabaForm, self).__init__(*args, **kwargs)
        self.fields['i_f_sh'].widget.attrs.update({'class': 'form-control'})
        self.fields['fakultet'].widget.attrs.update({'class': 'form-control'})
        self.fields['manzil'].widget.attrs.update({'class': 'form-control'})

