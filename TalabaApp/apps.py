from django.apps import AppConfig


class TalabaappConfig(AppConfig):
    name = 'TalabaApp'
